# Green Tea Water

Provide a calculator to help you produce water (by mixing) with a proper temperature for preparing green tea.

# Technologies used

Currently the site uses HTML, CSS and JavaScript. There used to be a version
of the site that relied on a WebAssembly module for calculating the mixing
ratio, but this was really overengineered.