var DEFAULTS = {
    TEMP_COLD: 20,
    TEMP_BOIL: 100,
    TEMP_TARGET: 70
};

var richmannGetM1 = function(t1, t2, tm, m2) {
    return ((t2 * m2) - (tm * m2)) / (tm - t1);
};

var formatNumber = function(number) {
    return number.toFixed(0);
};

var displayResult = function(coldWaterAmount, hotWaterAmount) {
    var result = document.querySelector('#result');
    result.style.display = 'inline-block';
    
    result.innerHTML = '<span class="highlight">Boil ' +
        formatNumber(hotWaterAmount) +
        ' ml of water</span><br>fill up the rest (' +
        formatNumber(coldWaterAmount) + ' ml) with cold water.';
};

var hideResult = function() {
    var result = document.querySelector('#result');
    result.innerHTML = '';
    result.style.display = 'none';
};

var init = function() {
    var calculator = document.querySelector('#calculator');
    calculator.style.display = 'block';
    
    var amount = document.querySelector('#amount');
    var valueDisplay = document.querySelector('#value');
    value.textContent = amount.value;
    amount.oninput = function inputHandler(e) {
        valueDisplay.textContent = e.target.value;
        
        hideResult();
    };
    
    var targetTemperature = document.querySelector('#target-temperature');
    targetTemperature.value = DEFAULTS.TEMP_TARGET;
    targetTemperature.oninput = hideResult;
    
    var coldTemperature = document.querySelector('#cold-temperature');
    coldTemperature.value = DEFAULTS.TEMP_COLD;
    coldTemperature.oninput = hideResult;
    
    var boilTemperature = document.querySelector('#boil-temperature');
    boilTemperature.value = DEFAULTS.TEMP_BOIL;
    boilTemperature.oninput = hideResult;
    
    var calculate = document.querySelector('#calculate');
    calculate.onclick = function clickhandler(_) {
        var hotWaterPart = 1;
        
        var coldWaterPart = richmannGetM1(
            coldTemperature.value,
            boilTemperature.value,
            targetTemperature.value,
            hotWaterPart);
        
        var totalWaterAmount = amount.value;
        
        var coldWaterAmount = totalWaterAmount / (coldWaterPart + hotWaterPart)
            * coldWaterPart;
        
        var hotWaterAmount = totalWaterAmount / (coldWaterPart + hotWaterPart)
            * hotWaterPart;
        
        displayResult(coldWaterAmount, hotWaterAmount);
    };  
};

window.onload = init;